import React from 'react'
import ReactDom from 'react-dom'
import Header from './component/Header/Header'
import Counter from './component/Counter/Counter'

const Main = () => (
    <div>
        <Header />
        <Counter />
    </div>
)

ReactDom.render(<Main />,document.getElementById('root'))
