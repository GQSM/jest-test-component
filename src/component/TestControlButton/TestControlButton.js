import React from 'react'

class TestControlButton extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            isClick: false,
        }
    }

    render() {
        return (
            <div>
                <input type="button" value={this.state.isClick ? '已點' : '點我'} onClick={() => { this.setState({isClick:true}) }} />
                <input type="button" value={this.state.isClick ? '已點' : '點我'} onClick={() => { this.setState({isClick:true}) }} />
            </div>
        )
    }
}

export default TestControlButton