import React, { useState, } from 'react'

const ControlButton = () => {

    const [isClick, changeClick] = useState(false)
    
    return (
        <div>
            <input type="button" value={isClick ? '已點' : '點我'} onClick={() => { changeClick(true) }} />
            <input type="button" value={isClick ? '已點' : '點我'} onClick={() => { changeClick(true) }} />
        </div>
    )
}

export default ControlButton