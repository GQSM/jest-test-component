import React from 'react'

// 建立另一個 Component 並將他與 Counter 一同 render
class Title extends React.Component {
    render() {
        return <p>計數器</p>
    }
}

class Counter extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            count: 0,
            name: '無名氏',
        }
    }

    render() {
        return (
            <div>
                <Title />
                請輸入姓名：
                <input
                    type="text"
                    value={this.state.name} 
                    onChange={(e) => { this.setState({name: e.target.value }) }} 
                />
                <br/>
                <span>{`${this.state.name}點了 ${this.state.count} 下`}</span>
                <br/>
                <button 
                    onClick={()=>{ this.setState({count: this.state.count + 1}) }}
                >
                    點我加 1
                </button>
            </div>
        )
    }
}

export default Counter