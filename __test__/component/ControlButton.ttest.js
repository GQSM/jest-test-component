import React from 'react';
import Enzyme, {shallow, mount} from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

Enzyme.configure({adapter: new Adapter()})

import ControlButton from '../../src/component/ControlButton/ControlButton'

it('<ControlButton />', () => {
  const controlButton = shallow(<ControlButton />)
  // 用find 從 Component 中找 Tag ，如果有相同的就用 at 來找第幾個 (從 0 開始)
  // Tag 上的屬性從 props() 中獲取，如果要取得 value 就找到該 DOM 用 .props.value 取得
  expect(controlButton.find('div').find('input').at(1).props().value).toBe('點我')
  // simulate 可以控制觸發 DOM 的動作，例如 'click'就會點擊
  controlButton.find('div').find('input').at(1).simulate('click')
  expect(controlButton.find('div').find('input').at(1).props().value).toBe('已點')
});