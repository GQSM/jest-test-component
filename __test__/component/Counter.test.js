import React from 'react'
import Enzyme, { shallow, mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

Enzyme.configure({ adapter: new Adapter() })

import Counter from '../../src/component/Counter/Counter'

describe('<Counter>', () => {
    // render component
    const counter = shallow(<Counter />)
    test('Check initial state', () => {
        expect(counter.state('count')).toBe(0)
        expect(counter.state('name')).toBe('無名氏')
    })

    test('After click event', () => {
        // 點擊 div 中的 button
        counter.find('div').find('button').simulate('click')
        expect(counter.state('count')).toBe(1)
    })

    test('Check sapn display text', () => {
        // 確認 div 中 span 內的文字
        expect(counter.find('div').find('span').text()).toBe('無名氏點了 1 下')
    })

    test('Change value of name', () => {
        expect(counter.find('div').find('input').props().value).toBe('無名氏')

        // 觸發 onChange 
        counter.find('div').find('input').simulate('change', { target: { value: '神Ｑ超人', }, })

        expect(counter.find('div').find('input').prop('value')).toBe('神Ｑ超人')
        expect(counter.find('div').find('span').text()).toBe('神Ｑ超人點了 1 下')
    })

    test('Get tag "p" in Counter', () => {
        const mountCounter = mount(<Counter />)
        expect(mountCounter.find('div').find('p').text()).toBe('計數器')
    })
})