import React from 'react'
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

Enzyme.configure({adapter: new Adapter()})

import TestControlButton from '../../src/component/TestControlButton/TestControlButton'

it('<TestControlButton />', () => {
  const testControlButton = shallow(<TestControlButton />)
  expect(testControlButton.find('div').find('input').at(1).props().value).toBe('點我')
  expect(testControlButton.state('isClick')).toBe(true)
  testControlButton.find('div').find('input').at(1).simulate('click')
  expect(testControlButton.find('div').find('input').at(1).props().value).toBe('已點')
});